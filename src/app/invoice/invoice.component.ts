import { Component, OnInit } from '@angular/core';
import {Invoice} from './Invoice';

@Component({
  selector: 'jce-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],
  inputs:['invoice']
})
export class InvoiceComponent implements OnInit {
Invoice:Invoice;
  constructor() { }

  ngOnInit() {
  }

}
