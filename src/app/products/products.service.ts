import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';

@Injectable()
export class ProductsService {
  productsobservable;

  getProducts(){
    this.productsobservable= this.af.database.list('/product').map(
      products=>{
        products.map(
          product=>{
            product.type = [];
              product.type.push(this.af.database.object('/category/'+product.categoryId) )      
          }
        );
        return products;
      }
    );

    return this.productsobservable;
  }


  // getProductsJoin(){
  //   this.productsJoinobservable = this.af.database.list('/product').map(
  //     products=>{
  //       products.map(
  //         product=>{
  //           product.type = [];
  //           for (var p in product.category){
  //             product.type.push(
  //               this.af.database.object('/category/'+p)
  //             )
  //           }
  //         }
  //       );
  //       return products;
  //     }
  //   );
  //   return this.productsJoinobservable;
  // }







  deleteProduct(product){
    let productKey = product.$key;
    this.af.database.object('/product/'+productKey).remove();
  }

  constructor(private af:AngularFire) { }

}
