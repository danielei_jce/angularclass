import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule,Routes} from '@angular/router';
import {AngularFireModule} from 'angularfire2';


import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { ProductComponent } from './product/product.component';
import { ProductsComponent } from './products/products.component';
import { ProductsService } from './products/products.service';
import { InvoicesService } from './invoices/invoices.service';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceComponent } from './invoice/invoice.component';
// import { JoinProductsComponent } from './join-products/join-products.component';

export const firebaseConfig = {
  apiKey: "AIzaSyC1HsJxRaL65cDbrNtgu90WaKZ3AsAbCh0",
  authDomain: "angularclass-cb958.firebaseapp.com",
  databaseURL: "https://angularclass-cb958.firebaseio.com",
  storageBucket: "angularclass-cb958.appspot.com",
  messagingSenderId: "275280476222"

}

const appRoutes:Routes = [
  {path:'users', component:UsersComponent},
  {path:'posts', component:PostsComponent},
  {path:'invoices', component:InvoicesComponent},
  {path:'invoicesForm', component:InvoiceFormComponent},
  {path:'products', component:ProductsComponent},
  {path:'', component:InvoicesComponent},
  {path:'**', component:PageNotFoundComponent}
]
@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent,
    ProductComponent,
    ProductsComponent,
    InvoiceFormComponent,
    InvoicesComponent,
    InvoiceComponent,
    // JoinProductsComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService,ProductsService, InvoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
