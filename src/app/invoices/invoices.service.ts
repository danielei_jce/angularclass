import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class InvoicesService {
  private _url = 'https://jsonplaceholder.typicode.com/invoice';
  //private _url = 'http://danielei.myweb.jce.ac.il/api/users';
  
   invoicesobservable;

   getInvoices() {
    // return this._http.get(this._url).map(res =>res.json()).delay(2000)
    this.invoicesobservable = this.af.database.list('/invoices');
    return this.invoicesobservable;

   }
   addInvoice(invoice){
     this.invoicesobservable.push(invoice);
   }
   

  constructor(private af:AngularFire) { }

}
