import { Component, OnInit } from '@angular/core';
import { InvoicesService } from './invoices.service';

@Component({
  selector: 'jce-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})
export class InvoicesComponent implements OnInit {
  isLoading:Boolean = true;
  invoices;
  currentInvoice;
  select(invoice){
    this.currentInvoice = invoice;
  }

  addInvoice(invoices){
    this._invoicesService.addInvoice(invoices);
  }
  constructor(private _invoicesService:InvoicesService) { }

  ngOnInit() {
    //this.invoices = this._invoicesService.getInvoices();
     this._invoicesService.getInvoices().subscribe(usersData => 
    {this.invoices = usersData;
      this.isLoading = false;
    console.log(this.invoices)});
  }

}
