import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Product} from './product';

@Component({
  selector: 'jce-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  inputs:['productVar', 'properrty']

})
export class ProductComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<Product>();
  productVar:Product;

  sendDelete(){
    this.deleteEvent.emit(this.productVar);
  }


  constructor() { }

  ngOnInit() {
  }

}
