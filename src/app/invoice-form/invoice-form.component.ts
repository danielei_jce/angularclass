import { Component, OnInit,Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Invoice} from '../invoice/invoice';
@Component({
  selector: 'jce-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.css']
})
export class InvoiceFormComponent implements OnInit {
  @Output() invoiceAddedEvent = new EventEmitter<Invoice>();
  invoiceF:Invoice = {name: '', count: ''};

  onSubmit(form:NgForm){
    console.log("Ok")
    console.log(form)
    this.invoiceAddedEvent.emit(this.invoiceF);
    this.invoiceF= {name: '', count: ''};
  }
  constructor() { }

  ngOnInit() {
  }

}
